#  function to query data in CSV in Amazon S3
def query_csv_s3(s3, bucket_name, filename, sql_exp, use_header):

    #  should we search by column name or column index
    if use_header:
        header = "Use"
    else:
        header = "None"
        
    #  query and create response
    resp = s3.select_object_content(
        Bucket=bucket_name,
        Key=filename,
        ExpressionType='SQL',
        Expression=sql_exp,
        InputSerialization = {'CSV': {"FileHeaderInfo": header}},
        OutputSerialization = {'CSV': {}},
    )
    
    #  upack query response
    records = []
    for event in resp['Payload']:
        if 'Records' in event:
            records.append(event['Records']['Payload'])  
        
    #  store unpacked data as a CSV format
    file_str = ''.join(req.decode('utf-8') for req in records)
    
    return file_str