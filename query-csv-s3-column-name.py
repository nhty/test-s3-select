import boto3
import importlib
import time
start_time = time.time()
query_csv_s3 = importlib.import_module("query-csv-s3")

#  start client with s3
s3 = boto3.client('s3', region_name='us-east-1')

#  define file location and name
bucket_name = 'testbucketselect'
filename = '1000000 Sales Records.csv'

#  create SQL expression to query by date using column names
sql_exp = ("SELECT Region, Country FROM s3object s WHERE Region = 'Sub-Saharan Africa' LIMIT 1")

#  should we use header names to filter
use_header = True

#  return CSV of unpacked data
file_str = query_csv_s3.query_csv_s3(s3, bucket_name, filename, sql_exp, use_header)

#  read CSV to dataframe
print(file_str)

print("--- %s seconds ---" % (time.time() - start_time))
